import { Subject } from 'rxjs/subject';
import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from './../shared/ingredient.model';

@Injectable()
export class RecipeService {

    recipesChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [
        new Recipe( 
            'Tasty Schnitzel', 
            'A super tasty schnitzel - just awesome!', 
            'http://icons.iconarchive.com/icons/blackvariant/button-ui-requests-9/512/DropZone-icon.png',
            [
                new Ingredient('Meat', 1),
                new Ingredient('French Fires', 20)
            ]
        ),
        new Recipe(
            'Big Fat Burger', 
            'I dont think you live without eating it', 
            'https://static1.squarespace.com/static/556c9b57e4b075c33164e8f4/t/557483c7e4b07ff6e579167f/1433699272669/',
            [
                new Ingredient('Buns', 2),
                new Ingredient('Meat', 1)
            ]
        )
      ];

      constructor(private slService: ShoppingListService) {}


      setRecipes(recipes: Recipe[]) {
          this.recipes = recipes;
          this.recipesChanged.next(this.recipes.slice());
      }

      getRecipes() {
        return this.recipes.slice();
      }

      getRecipe(index: number) {
          return this.recipes[index];
      }

      addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
      }

      addRecipe(recipe: Recipe){
          this.recipes.push(recipe);
          this.recipesChanged.next(this.recipes.slice());
      }

      updateRecipe(index: number, recipe: Recipe) {
          this.recipes[index] = recipe;
          this.recipesChanged.next(this.recipes.slice());
      }

      deleteRecipe(index: number) {
          this.recipes.splice(index, 1);
          this.recipesChanged.next(this.recipes.slice());
      }
}